import React, { useState } from "react";
import MaterialTable from "material-table";
import DeleteIcon from "@material-ui/icons/Delete";
import SearchIcon from "@material-ui/icons/Search";
import SaveIcon from "@material-ui/icons/Save";
import { Button } from "@material-ui/core";

import "./styles.css";

export default function App() {
  const [dataStore, setDataStore] = useState([
    { name: "Cormac", job: "Physician", age: 35 },
    { name: "Eden", job: "Physicist", age: 30 },
    { name: "Mac", job: "Anesthesiologist", age: 35 },
    { name: "Henwick", job: "Acutary", age: 35 },
    { name: "Thune", job: "U.S. Senator", age: 35 },
    { name: "Casey", job: "Physician", age: 35 },
    { name: "Darjeel", job: "Physician", age: 35 },
  ]);

  return (
    <div className="App">
      <h1>Material-Table Demo</h1>
      <div style={{ maxWidth: "100%", paddingTop: "12px" }}>
        <MaterialTable
          columns={[
            {
              title: "Name",
              field: "name",
              headerStyle: {
                backgroundColor: "green"
              }
            },
            {
              title: "Occupation",
              field: "job",
              headerStyle: {
                backgroundColor: "blue"
              }
            },
            {
              title: "Age",
              field: "age",
              type: "numeric",
              headerStyle: {
                backgroundColor: "red"
              }
            }
          ]}
          data={dataStore}
          title="Material-Table Demo"
          icons={{
            Clear: (props) => <DeleteIcon />,
            Search: (props) => <SearchIcon />,
            ResetSearch: (props) => <DeleteIcon />
          }}
          actions={[
            {
              icon: () => <SaveIcon />,
              tooltip: "Save User",
              onClick: (event, rowData) => alert("You saved " + rowData.name)
            }
          ]}
          components={{
            Action: (props) => (
              <Button
                onClick={(event) => props.action.onClick(event, props.data)}
                color="primary"
                variant="text"
                style={{ textTransform: "none" }}
                size="small"
              >
                Save
              </Button>
            )
          }}
          options={{
            headerStyle: {
              backgroundColor: "#01579b",
              color: "#FFF"
            }
          }}
        />
      </div>
    </div>
  );
}
